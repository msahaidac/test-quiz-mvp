package com.example.testquiz.repository;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.testquiz.repository.dao.QuizDao;
import com.example.testquiz.data.model.Quiz;

@Database(entities = {Quiz.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {
    public abstract QuizDao quizDao();
}
