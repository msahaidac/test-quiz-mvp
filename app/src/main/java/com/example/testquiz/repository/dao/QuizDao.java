package com.example.testquiz.repository.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.testquiz.data.model.Quiz;

import java.util.List;

@Dao
public interface QuizDao {

    @Insert
    public void addQuiz(Quiz quiz);

    @Query("select * from quiz")
    public List<Quiz> getQuiz();



    @Query("select * from quiz where quiz.question like :text")
    public Quiz getQuizByQuestion(String text);
}
