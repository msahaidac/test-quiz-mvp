package com.example.testquiz.screen.addQuiz;

public interface AddQuizView {

    void showResult(String message);
    void resetFields();
}
