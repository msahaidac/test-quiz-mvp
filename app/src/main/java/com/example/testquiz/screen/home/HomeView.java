package com.example.testquiz.screen.home;

public interface HomeView {

    public void switchToAddQuizScreen();
    public void switchToPassQuizScreen();
}
