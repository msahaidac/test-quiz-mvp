package com.example.testquiz.screen.addQuiz;

import com.example.testquiz.data.model.Quiz;
import com.example.testquiz.screen.home.MainActivity;
import com.example.testquiz.utils.QuizUtils;

import java.util.Optional;

public class AddQuizPresenter {

    private AddQuizView addQuizView;


    public AddQuizPresenter(AddQuizView addQuizView) {
        this.addQuizView = addQuizView;
    }

    public void insertQuiz(Quiz insertedQuiz){
        if (QuizUtils.checkIfQuizExist(insertedQuiz.getQuestion())){
            addQuizView.showResult("This quiz is already exist!");
        } else {
            Quiz quiz = new Quiz();
            quiz.setId(insertedQuiz.getId());
            quiz.setQuestion(insertedQuiz.getQuestion());
            quiz.setFirst_answer(insertedQuiz.getFirst_answer());
            quiz.setSecond_answer(insertedQuiz.getSecond_answer());
            quiz.setThird_answer(insertedQuiz.getThird_answer());
            quiz.setFourth_answer(insertedQuiz.getFourth_answer());
            quiz.setCorrect_answer(insertedQuiz.getCorrect_answer());

            MainActivity.myDatabase.quizDao().addQuiz(quiz);

            addQuizView.showResult("Quiz was added!");
        }
    }
}
