package com.example.testquiz.screen.passQuiz;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.testquiz.screen.home.MainActivity;
import com.example.testquiz.R;
import com.example.testquiz.data.model.Quiz;

import java.util.List;

public class PassQuiz extends AppCompatActivity {
    private RadioButton first, second, third, fourth, radioButton;
    private RadioGroup radioGroup;
    private String final_answer;
    private TextView Question;
    private Button viewResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_quiz);


        List<Quiz> quiz = MainActivity.myDatabase.quizDao().getQuiz();

        Question = findViewById(R.id.question);
        first = findViewById(R.id.first_answer);
        second = findViewById(R.id.second_answer);
        third = findViewById(R.id.third_answer);
        fourth = findViewById(R.id.fourth_answer);
        radioGroup = findViewById(R.id.group_radioBtn);
        viewResult = findViewById(R.id.view_result_btn);


        for (Quiz qz : quiz) {
            String question = qz.getQuestion();
            String first_a = qz.getFirst_answer();
            String second_a = qz.getSecond_answer();
            String third_a = qz.getThird_answer();
            String fourth_a = qz.getFourth_answer();
            String correct_a = qz.getCorrect_answer();


            Question.setText(question);
            first.setText(first_a);
            second.setText(second_a);
            third.setText(third_a);
            fourth.setText(fourth_a);
            final_answer = correct_a;

        }

        viewResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedRadioBtn= radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedRadioBtn);
                String answer = radioButton.getText().toString();
                if (final_answer.equalsIgnoreCase(answer)) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(PassQuiz.this);
                        builder.setIcon(R.drawable.happy_emoji);
                        builder.setTitle("Correct Answer");
                        builder.setMessage("Not so bad!!!");
                        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });

                        AlertDialog alertDialog = builder.show();
                    } else {
                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(PassQuiz.this);
                    builder1.setIcon(R.drawable.mad_emoji);
                    builder1.setTitle("Wrong Answer");
                    builder1.setMessage("Keep trying!!!");
                    builder1.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });

                    AlertDialog alertDialog1 = builder1.show();
                }
            }
        });


    }
}
