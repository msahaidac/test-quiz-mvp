package com.example.testquiz.screen.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.testquiz.R;
import com.example.testquiz.repository.MyDatabase;
import com.example.testquiz.screen.addQuiz.AddQuiz;
import com.example.testquiz.screen.passQuiz.PassQuiz;

public class MainActivity extends AppCompatActivity implements HomeView, View.OnClickListener{
    Button addQuizBtn, startQuizBtn;
    public static MyDatabase myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    initDb();
    initViewElements();

    }


    private void initViewElements(){
        addQuizBtn = findViewById(R.id.add_quiz_btn);
        addQuizBtn.setOnClickListener(this);
        startQuizBtn = findViewById(R.id.start_quiz_btn);
        startQuizBtn.setOnClickListener(this);
    }

    private void initDb(){
        myDatabase = Room.databaseBuilder(getApplicationContext(),MyDatabase.class,"quizdb").allowMainThreadQueries().build();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_quiz_btn:

                startActivity(new Intent(MainActivity.this, AddQuiz.class));
                break;

            case R.id.start_quiz_btn:
                startActivity(new Intent(MainActivity.this, PassQuiz.class));
                break;
        }
    }

    @Override
    public void switchToAddQuizScreen() {

    }

    @Override
    public void switchToPassQuizScreen() {

    }
}
