package com.example.testquiz.screen.addQuiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.testquiz.R;
import com.example.testquiz.data.model.Quiz;
import com.example.testquiz.screen.home.MainActivity;
import com.example.testquiz.screen.passQuiz.PassQuiz;

public class AddQuiz extends AppCompatActivity implements AddQuizView {
    private EditText question, id, first_answer, second_answer, third_answer, fourth_answer, correct_answer;
    private Button addQuizBtn;
    private AddQuizPresenter addQuizPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_quiz);

        initViewElements();

        addQuizBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addQuizPresenter.insertQuiz(getQuizFromFields());
            }
        });
    }

    private Quiz getQuizFromFields(){
        Quiz quiz = new Quiz();
        quiz.setId(Integer.parseInt(id.getText().toString()));
        quiz.setQuestion(question.getText().toString());
        quiz.setFirst_answer(first_answer.getText().toString());
        quiz.setSecond_answer(second_answer.getText().toString());
        quiz.setSecond_answer(third_answer.getText().toString());
        quiz.setSecond_answer(fourth_answer.getText().toString());
        quiz.setSecond_answer(correct_answer.getText().toString());
        return quiz;
    }

    private void initViewElements(){
        id = findViewById(R.id.id);
        question = findViewById(R.id.question);
        first_answer = findViewById(R.id.first_answer);
        second_answer = findViewById(R.id.second_answer);
        third_answer = findViewById(R.id.third_answer);
        fourth_answer = findViewById(R.id.fourth_answer);
        correct_answer = findViewById(R.id.correct_answer);
        addQuizBtn = findViewById(R.id.add_btn);
        addQuizPresenter = new AddQuizPresenter(this);
    }

    @Override
    public void showResult(String message) {
        Toast.makeText(AddQuiz.this,message, Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void resetFields() {
        id.setText("");
        question.setText("");
        first_answer.setText("");
        second_answer.setText("");
        third_answer.setText("");
        fourth_answer.setText("");
        correct_answer.setText("");
    }
}
