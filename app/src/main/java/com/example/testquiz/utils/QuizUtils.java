package com.example.testquiz.utils;

import com.example.testquiz.data.model.Quiz;
import com.example.testquiz.screen.home.MainActivity;

public class QuizUtils {

    public static boolean checkIfQuizExist(String quizQuestion){
        Quiz existingQuiz = MainActivity.myDatabase.quizDao().getQuizByQuestion(quizQuestion);
        return existingQuiz != null;
    }
}
