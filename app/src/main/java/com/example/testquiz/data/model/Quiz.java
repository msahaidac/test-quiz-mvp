package com.example.testquiz.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "quiz")
public class Quiz {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "question")
    private String question;

    @ColumnInfo(name = "first_answer")
    private String first_answer;

    @ColumnInfo(name = "second_answer")
    private String second_answer;

    @ColumnInfo(name = "third_answer")
    private String third_answer;

    @ColumnInfo(name = "fourth_answer")
    private String fourth_answer;

    @ColumnInfo(name = "correct_answer")
    private String correct_answer;

    @ColumnInfo(name = "status")
    private boolean status;

    public void setId(int id) {
        this.id = id;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setFirst_answer(String first_answer) {
        this.first_answer = first_answer;
    }

    public void setSecond_answer(String second_answer) {
        this.second_answer = second_answer;
    }

    public void setThird_answer(String third_answer) {
        this.third_answer = third_answer;
    }

    public void setFourth_answer(String fourth_answer) {
        this.fourth_answer = fourth_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getFirst_answer() {
        return first_answer;
    }

    public String getSecond_answer() {
        return second_answer;
    }

    public String getThird_answer() {
        return third_answer;
    }

    public String getFourth_answer() {
        return fourth_answer;
    }

    public String getCorrect_answer() {
        return correct_answer;
    }

    public boolean isStatus() {
        return status;
    }
}
